<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\SoftDeletes;



class MensajeUsuario extends Model
{
    use SoftDeletes;
	protected $dates = ['deleted_at'];
    
	protected $table = 'mensajeusuario';
	protected $dates = ['deleted_at'];
    public $timestamps = false;

	public function usersEmisor(){
		
		return $this->belongsTo('App\User', 'users_id_emisor');
	
	}

	public function usersReceptor(){
		
		return $this->belongsTo('App\User', 'users_id_receptor');
	
	}
	
}
