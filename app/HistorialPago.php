<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class HistorialPago extends Model
{
    use SoftDeletes;
	protected $dates = ['deleted_at'];
	
    protected $table = 'historialPago';

    public function usuario(){
    	
    	return $this -> belongsTo('App\User');

    }
}
