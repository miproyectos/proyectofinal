<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Partida extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at',
    'fecha_estreno',
    'fecha_final'];
    
    
    protected $table = 'partida';
    
    public function usuarioPartidaApuesta(){
    	
    	return $this -> hasMany('App\UsuarioPartidaApuesta');
    
    }

    public function juego(){

    	return $this -> belongsTo('App\Juego');

    }

    public function ganador(){

    	return $this -> belongsTo('App\Equipo', 'ganador');

    }    

    public function equipoa(){

        return $this -> belongsTo('App\Equipo', 'equipoa');

    }    

    public function equipob(){

        return $this -> belongsTo('App\Equipo', 'equipob');

    }    


}
