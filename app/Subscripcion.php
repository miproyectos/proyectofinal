<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Subscripcion extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    
    //
    protected $table = 'subscripcion';

    public function usuario(){
    	
    	return $this -> belongsTo('App\User');

    }

    public function juego(){
    	
    	return $this -> belongsTo('App\Juego');

    }

    
}
