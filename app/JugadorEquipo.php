<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JugadorEquipo extends Model
{
    use SoftDeletes;
	protected $dates = ['deleted_at'];

	
    protected $table = 'JugadorEquipo';

    
    public function jugador(){
    	
    	return $this -> belongsTo('App\Jugador');

    }

    public function equipo(){
    	
    	return $this -> belongsTo('App\Equipo');

    }

}
