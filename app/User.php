<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;


class User extends Authenticatable
{
    use SoftDeletes;

    protected $table = 'users';
    protected $dates = ['deleted_at'];
    public $timestamps = false;


    public function mensajeusuario() {
        
        return $this->hasMany('App\MensajeUsuario');
    
    }
    public function usuariopartidaapuesta() {
        
        return $this->hasMany('App\UsuarioPartidaApuesta');
    
    }
    public function subscripcion() {

        return $this->hasMany('App\Subscripcion');
   
    }
    public function chatmensaje() {
   
        return $this->hasMany('App\ChatMensaje');
   
    }
    public function evento() {
   
        return $this->hasMany('App\Evento');
   
    }
    public function historialpago() {
   
        return $this->hasMany('App\HistorialPago');
   
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
