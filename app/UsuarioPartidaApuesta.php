<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;


class UsuarioPartidaApuesta extends Model
{
    use SoftDeletes;

    protected $table = 'usuariopartidaapuesta';
    protected $dates = ['deleted_at'];
    public $timestamps = false;


    public function usuario(){
        
        return $this->belongsTo('App\User');
    
    }
    public function partida(){
        
        return $this->belongsTo('App\Partida');
    
    }
 
}
