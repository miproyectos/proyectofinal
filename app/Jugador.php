<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Jugador extends Model
{
    use SoftDeletes;
	protected $dates = ['deleted_at'];
	
    //
    protected $table = 'jugador';
    
    public function jugadorPartida(){
    	
    	return $this -> hasMany('App\JugadorEquipo');
    
    }
}
