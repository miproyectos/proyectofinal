<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Equipo extends Model
{
    use SoftDeletes;
	protected $dates = ['deleted_at'];
	
    protected $table = 'equipo';

    public function jugadorequipo(){
    	
    	return $this -> hasMany('App\JugadorEquipo');

    }
    public function partida(){
        
        return $this -> hasMany('App\Partida');

    }

}
