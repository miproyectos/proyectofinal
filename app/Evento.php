<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Evento extends Model
{
    use SoftDeletes;

	protected $dates = ['deleted_at'
	,'fecha_anunciar'
	,'fecha_inicio'
	,'fecha_final'];	
    protected $table = 'evento';
    public $timestamps = false;
    

    public function usuario(){
    	
    	return $this -> belongsTo('App\User');

    }

}
