<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Juego;
use App\Partida;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Str;

class Competiciones extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($tipo_competicion=null)
    {
        $juegos = Juego::lists('nombre','id');

        if($tipo_competicion==null) {
            return view('competiciones', [
                'juegos' => $juegos,
                ]);
        }
        else
        {
            return view('competiciones', [
                'juegos' => $juegos,
                'tipo_competicion' => $tipo_competicion
                ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *Carbon
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    /**
     * funcion auto completar recogiendo el titulos de las competiciones
     *
     * @return  titulo de las competiciones [<description>]
     */
    public function autocompletar_titulos()
    {
        $term = Str::lower(Input::get('term'));

        $return_array = [];

        $queries = Partida::where('titulo','LIKE','%'.$term.'%')->get();

        foreach ($queries as $query) {
            $return_array[] = [ 'id' => $query->id, 'value' => $query->titulo];
        }
        return Response::json($return_array);
    }






    /**
     * filtra competiciones
     *
     * 
     * @return $partidas partidas pasadas por el filtro
     */
    public function json_filtro_competiciones()
    {
        setlocale(LC_TIME, 'es_ES.utf8');

        $date = Carbon::now()->setTimezone('GMT+2')->format('Y-m-d H:i:s');

        $query = Partida::where("fecha_estreno","<",$date)->orderby('fecha_inicio');

        $u_input = filter_input_array(INPUT_POST,[
            'titulo' => FILTER_SANITIZE_STRING, 
            'tipo_competicion' => FILTER_SANITIZE_NUMBER_INT,
            'juego' => FILTER_SANITIZE_STRING,
            'limite_apuesta' => FILTER_SANITIZE_NUMBER_INT,
            'estado' => FILTER_SANITIZE_NUMBER_INT,
            'equipo' => FILTER_SANITIZE_STRING,
            'fecha_inicio' => FILTER_SANITIZE_STRING,
            'fecha_final' => FILTER_SANITIZE_STRING

        ]);


        // filtro
        //
        if(!empty($u_input['titulo']))
        {
            $query->where("titulo",'like','%'.$u_input['titulo'].'%');
        }
        
        if(!empty($u_input['tipo_competicion']))
        {
            $query->where("tipo_partida", $u_input['tipo_competicion']);
        } 
        
        
        if(!empty($u_input['juego']))
        {
        
            // si queremos no comparar por la id si no por el nombre del juego
            //$query->whereHas('juego',function($q) use ($u_input)
            //{
            //    return $q->where('nombre',$u_input['juego']);
            //})
            //->with(['juego'])->get();

            $query->where("juego_id", $u_input['juego']);
        
        }

        if(!empty($u_input['estado']))
        {
            $query->where("estado", $u_input['estado']);
        }
        
        if(!empty($u_input['fecha_inicio']))
        {
            $query->where("fecha_inicio", ">", $u_input['fecha_inicio']);
        }
        
        if(!empty($u_input['fecha_final']))
        {
            $query->where("fecha_final", "<", $u_input['fecha_final']);
        }

        if(!empty($u_input['equipo']))
        {

            $query->where("equipo", $u_input['equipo']);

        }
        

        $partidas = $query->get()->map(function($q){
            return [
                'id' => $q->id,
                'titulo' => $q->titulo,
                'nombre_juego' => $q->juego->nombre
            ];
        });

        // sacamos solo los datos que nos interesen mpero mejor usar map
        /*foreach($partidas as $partida) 
        {

            $obj = new stdClass;
            $obj->nombre_juego = $partida->juego->nombre;
            $obj->titulo  = $partida->titulo;
            $obj->descripcion  = $partida->descripcion;

            array_push($resultado, $obj);
        }*/

        return $partidas;
    }
}
