<?php
namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;
use App\Evento;
use App\Partida;
use App\Ruta;

class Portada extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        setlocale(LC_TIME, 'es_ES.utf8');

        //$date = Carbon::now()->setTimezone('GMT+2')->formatLocalized('%e %B %Y');
        $date = Carbon::now()->setTimezone('GMT+2')->format('Y-m-d H:i:s');

        $usuarios = User::get();
        
        $eventos = Evento::where('fecha_final','>',$date)->orderBy('fecha_anunciar')->limit(6)->get();


        $ultipartida = Partida::get();
        
        // $url =  asset("imagenes/cup.png");

        // die(Ruta::rutaAbsoluta('evento'));


        return view('portada', [
            'usuarios' => $usuarios,
            'eventos' => $eventos,
            'ultipartida' => $ultipartida
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('nuevoMensajeUsuario');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
