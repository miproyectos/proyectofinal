<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
// Authentication routes...
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');



//-----------------PAGINAS-------------------------


Route::get('/', 'Portada@index');
Route::get('juegos', 'Juegos@index');

Route::get('competiciones', 'Competiciones@index');
Route::get('competiciones/{tipo_competicion}', 'Competiciones@index');

Route::get('vercompeticion/{id}', 'VerCompeticion@index');


//-----------------FUNCIONES-------------------------

//funciones competiciones
Route::post('async/filtrocompeticiones/', 'Competiciones@json_filtro_competiciones');
Route::get('async/mostrarcompeticionesauto', 'Competiciones@autocompletar_titulos');
