<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Juego extends Model
{

    use SoftDeletes;
	protected $dates = ['deleted_at'];
	
    protected $table = 'juego';

    public function partida(){
    	
    	return $this -> hasMany('App\Partida');

    }

    public function subscripcion(){
    	
    	return $this -> hasMany('App\Subscripcion');

    }
}
