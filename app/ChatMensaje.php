<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class ChatMensaje extends Model
{
    use SoftDeletes;
	protected $dates = ['deleted_at'];
    protected $table = 'chatMensaje';

    public function usuario(){
    	
    	return $this -> belongsTo('App\User');

    }

    public function partida(){
    	
    	return $this -> belongsTo('App\Partida');
    
    }
}
