<?php

namespace App;

class Ruta
{
	const evento = 'media/eventos/';
    const partida = 'media/partida/';
	
    public static function rutaAbsoluta($elemento) {

        return self::raiz().constant('self::'.$elemento);
    }

     public static function rutaRelativa($elemento) {

        return constant('self::'.$elemento);
    }

    private static function raiz() {

        return realpath(dirname(__FILE__).'/../').'/';
    }

}
