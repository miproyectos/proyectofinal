<?php
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

class Chat implements MessageComponentInterface
{
	protected $clients;

    public function __construct() {
        $this->clients = new \SplObjectStorage;
    }

    public function onOpen(ConnectionInterface $conn)
    {
		$colores = array('007AFF','FF7000','FF7000','15E25F','CFC700','CFC700','CF1100','CF00BE','F00');
		$color   = $colores[array_rand($colores)];

		$conn->coloruser = $color;

    	// Store the new connection to send messages to later
        $this->clients->attach($conn);

        $conn->send(json_encode(array('code' => 'set_color', 'color' => $color)));

        echo "New connection! ({$conn->resourceId}) COLOR: {$color}\n";
    }

    public function onMessage(ConnectionInterface $from, $msg)
    {
 	    $numRecv = count($this->clients) - 1;

    	$mensaje = json_decode($msg, true);

    	/**
    	 * Mandar mensajes
    	 */
    	if($mensaje['code'] == 'send_message')
    	{
			$mensaje['color']  = $from->coloruser;
			$mensaje['emisor'] = $from->nombreusuario;

	        foreach ($this->clients as $client) 
	        {
	        	if ($from !== $client)
	        	{
	            	$client->send(json_encode($mensaje));
	        	}
	        }

	        echo sprintf('Connection %d sending message "%s" to %d other connection%s' . "\n"
	            , $from->resourceId, $msg, $numRecv, $numRecv == 1 ? '' : 's');
    	}
    	/**
    	 * Mostrar usuarios en la consola
    	 */
    	else if($mensaje['code'] == 'show_users_console')
    	{
    		foreach($this->clients as $c)
    		{
    			print_r("Resource: ".$c->resourceId."\r\n");
    			print_r("Color: ".$c->coloruser."\r\n");
    		}
    	}
    	/*
    	 *loggin
    	 */
    	else if($mensaje['code'] == 'loggin')
    	{
			$from->nombreusuario = $mensaje['name'];

			$this->updateUserlist();
    	}
    }

    public function onClose(ConnectionInterface $conn) {
    	// The connection is closed, remove it, as we can no longer send it messages
        $this->clients->detach($conn);

        $this->updateUserlist();

        echo "Connection {$conn->resourceId} has disconnected\n";
    }

    public function onError(ConnectionInterface $conn, \Exception $e) {
    	
    	$mensajeerror = "An error has occurred: {$e->getMessage()}\n";
    	echo $mensajeerror;

        $client->send($mensajeerror);
        $conn->close();
    }

    protected function updateUserlist()
    {
    	$mensaje = [
			"code"     => "show_users",
			"usuarios" => []
    	];

    	/**
    	 * saca todos los usuarios conectados
    	 */
        foreach ($this->clients as $client)
        {
        	array_push($mensaje["usuarios"], $client->nombreusuario);
        }
        
        /**
         * mandamos a todos los usuarios conectados los usuarios que hay conectados
         */
        foreach ($this->clients as $client) 
        {
        	$client->send(json_encode($mensaje));
        }
    }
}