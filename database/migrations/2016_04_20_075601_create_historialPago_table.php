<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistorialPagoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historialpago', function (Blueprint $table) {
            $table->increments('historial_pago')->unsigned();
            $table->integer("users_id")->unsigned();
            $table->string("transaccion");
            $table->integer("cantidad");
            $table->timestamps();
            $table->softDeletes();
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Schema::rename('historialpago','historialpago_copy');
        Schema::dropIfExists('historialpago');
    }
}
