<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartidaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partida', function (Blueprint $table) {
            $table->increments("id")->unsigend();
            $table->integer("juego_id")->unsigned();
            $table->integer("lote");
            $table->integer("limite_user");
            $table->integer("limite_apuesta");
            $table->string("equipoa");
            $table->string("equipob");
            $table->string("ganador");
            $table->string("twith_url");
            $table->string("imagen");
            $table->timestamp("fecha_estreno");
            $table->timestamp("fecha_final");
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Schema::rename('partida','partida_copy');
        Schema::dropIfExists('partida');
    }
}
