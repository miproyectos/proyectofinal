<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evento', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer("users_id")->unsigned();
            $table->string("imagen");
            $table->string("titulo");
            $table->string("descripcion1");
            $table->string("descripcion2");
            $table->string("filtro");
            $table->timestamp("fecha_anunciar");
            $table->timestamp("fecha_final");
            $table->timestamps();
            $table->softDeletes();
        });

        // Insert some stuff
        DB::table('evento')->insert([
                'fecha_anunciar' => '2016-04-23 13:44:49',
                'fecha_final' => '2016-04-23 13:44:49',

        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('evento');
    }
}
