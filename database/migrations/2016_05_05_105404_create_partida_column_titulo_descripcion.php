<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartidaColumnTituloDescripcion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('partida', function (Blueprint $table) {
            $table->string("titulo")->after("ganador");
            $table->string("descripcion")->after("ganador");

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('partida', function (Blueprint $table) {
            
            $table->dropColumn("titulo");
            $table->dropColumn("descripcion");
        });
    }
}
