<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangePartidaColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::table('partida', function (Blueprint $table) {

            $table->timestamp("fecha_inicio")->after("fecha_estreno");

        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('partida', function (Blueprint $table) {
            
            $table->dropColumn("fecha_inicio");
        
        });
    }
}
