<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChatMensajeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chatmensaje', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer("users_id")->unsigned();
            $table->integer("partida_id")->unsigned();
            $table->string("mensaje");
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chatmensaje');
    }
}
