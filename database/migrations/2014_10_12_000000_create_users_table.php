<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {

            $table->increments('id');
            $table->string('nombre_usuario')->unique();
            $table->string('nombre');
            $table->integer('tipo')->unsigned()->default(0);
            $table->integer('coins')->unsigned()->default(0);
            $table->string('email')->unique();
            $table->string('imagen');
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();

        });

        // Insert some stuff
        DB::table('users')->insert([
                'nombre_usuario' => 'admin',
                'nombre' => 'Juan Leopoldo Díaz Sánchez',
                'tipo' => 1,
                'coins' => 0,
                'email' => 'info5hax@gmail.com',
                'password' => "123456"

        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Schema::rename('users','users_copy');
        Schema::dropIfExists('users');
    }
}
