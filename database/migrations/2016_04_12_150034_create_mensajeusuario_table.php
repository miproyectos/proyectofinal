<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class CreateMensajeusuarioTable extends Migration
{
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mensajeusuario', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('users_id_emisor');
            $table->string('users_id_receptor');
            $table->string('titulo');
            $table->string('mensaje');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('mensajeusuario');
    }
}
