<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTipopartida extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::table('partida', function (Blueprint $table) {
            $table->integer("tipo_partida")->after("imagen");

        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('partida', function (Blueprint $table) {
            
            $table->dropColumn("tipo_partida");
        
        });
    }
}
