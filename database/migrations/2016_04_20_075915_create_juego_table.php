<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJuegoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('juego', function (Blueprint $table) {
            $table->increments("id")->unsigned();
            $table->string("nombre");
            $table->string("categoria");
            $table->string("descripcion");
            $table->string("imagen");
            $table->string("url_video");
            $table->timestamps();
            $table->softDeletes();
        });
        
        // Insert some stuff
        DB::table('juego')->insert([
            ['nombre' => 'Hero Of Storm', 'categoria' => 'moba'],
            ['nombre' => 'Team Fortress 2', 'categoria' => 'shooter'],
            ['nombre' => 'Counter Strike', 'categoria' => 'shooter'],
            ['nombre' => 'Lenguage of Legends', 'categoria' => 'moba'],
            ['nombre' => 'smite', 'categoria' => 'moba']
        ]);
                
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('juego');
    }
}
