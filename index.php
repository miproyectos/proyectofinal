<!DOCTYPE html>
<html>
<head>
<meta charset='UTF-8' />
<style type="text/css">
<!--
.chat_wrapper {
	width: 500px;
	margin-right: auto;
	margin-left: auto;
	background: #CCCCCC;
	border: 1px solid #999999;
	padding: 10px;
	font: 12px 'lucida grande',tahoma,verdana,arial,sans-serif;
}
.chat_wrapper .message_box {
	background: #FFFFFF;
	height: 150px;
	overflow: auto;
	padding: 10px;
	border: 1px solid #999999;
}
.chat_wrapper .panel input{
	padding: 2px 2px 2px 5px;
}
.system_msg{color: #BDBDBD;font-style: italic;}
.user_name{font-weight:bold;}
.user_message{color: #88B6E0;}
-->
</style>
	<!-- Add jQuery library -->
	<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>

	<!-- Add mousewheel plugin (this is optional) -->
	<script type="text/javascript" src="fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>
	<script type="text/javascript" src="bootstrap/bootstrap.js"></script>


	<!-- Add fancyBox -->
	<link rel="stylesheet" href="fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
	<script type="text/javascript" src="fancybox/source/jquery.fancybox.pack.js?v=2.1.5"></script>

	<link rel="stylesheet" href="fancybox/jquery.fancybox-1.3.4.css" type="text/css" media="screen" /> 
	<link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="bootstrap/bootstrap.min.css">
</head>
<body>	

<script language="javascript" type="text/javascript">  
$(document).ready(function(){
	
	var nomusuario = "";

	$("#loggin").fancybox({
		'modal': true,
	});

	$("#loggin").trigger('click');				


	/*
	notificaciones de chrome
	 */
	Notification.requestPermission().then(function(result) {
  		console.log(result);
	});

	function notifyMe() {
	  // Let's check if the browser supports notifications
	  if (!("Notification" in window)) {
	    alert("This browser does not support system notifications");
	  }

	  // Let's check whether notification permissions have already been granted
	  else if (Notification.permission === "granted") {
	    // If it's okay let's create a notification
	    var notification = new Notification("Hi there!");
	  }

	  // Otherwise, we need to ask the user for permission
	  else if (Notification.permission !== 'denied') {
	    Notification.requestPermission(function (permission) {
	      // If the user accepts, let's create a notification
	      if (permission === "granted") {
	        var notification = new Notification("Hi there!");
	      }
	    });
	  }

	  // Finally, if the user has denied notifications and you 
	  // want to be respectful there is no need to bother them any more.
	}

	function notifyMe(theBody,theIcon,theTitle) {
	  if(document.hidden){
		  if (Notification.permission === "granted") {
		    	  var options = {
			      body: theBody,
			      icon: theIcon
			  }
			  var n = new Notification(theTitle,options);
			  setTimeout(n.close.bind(n), 5000); 
			  } 	
	  }	

	}
	//create a new WebSocket object.
	var wsUri = "ws://localhost:8080/chat/servidor.php"; 	
	window.websocket = new WebSocket(wsUri);
	window.chatColor = '000000';

	websocket.onmessage = function(e){

		var obj = JSON.parse(e.data);

		if(obj.code == "send_message"){
			
			notifyMe(obj.message,"img.jpg",obj.name);

			$("#message_box").append("<p><span style='color: #"+obj.color+"' >" + obj.emisor + "</span> : " + obj.message + "</p>");
		}
		else if(obj.code == "set_color")
		{
			window.chatColor = obj.color;
		}
		else if(obj.code == "show_users"){

			$("#usuarios").html(obj.usuarios.join("<br>"));
		}
	}

	websocket.onclose = function(){   	

		$("#message_box").append("<p>se perdio la conexion con el servidor de chat</p>");
	}

	websocket.onerror = function(e){
		
		var obj = JSON.parse(e.data);    	

		$("#message_box").append("<p>"+e+"</p>");

	}
	
	$("#enviar_nombreusuario").click(function(){

		if($("#nombreusuario").val() != ""){

			nomusuario= $("#nombreusuario").val();
			//prepare json data
			var msg = {
				code: 'loggin',
				name: nomusuario,
			};
			//convert and send data to server
			websocket.send(JSON.stringify(msg));

			$.fancybox.close();	
		}
	});

	$('#send-btn').click(function(a){ //use clicks message send button	

		a.preventDefault();

		var mymessage = $('#message').val(); //get message text
		
		if(nomusuario == ""){ //empty name?
			alert("Enter your Name please!");
			return;
		}
		if(mymessage == ""){ //emtpy message?
			alert("Enter Some message Please!");
			return;
		}

		//prepare json data
		var msg = {
			code: 'send_message',
			message: mymessage,
		};
		//convert and send data to server
		websocket.send(JSON.stringify(msg));

		$("#message_box").append("<p><span style='color: #" + window.chatColor + "' >" + nomusuario + "</span> : " + mymessage + "</p>");
	});

});
</script>
<div class="chat_wrapper">
<div class="message_box" id="message_box"></div>
<div class="panel">

<form action="">
	<input type="text" name="message" id="message" placeholder="Message" maxlength="80" style="width:60%" />
	<button type="submit" id="send-btn">Send</button>
</form>
</div>
<h2>usuarios</h2>
<hr>
<div id="usuarios">
	
</div>
</div>
<a id="loggin" href="#data"></a>
<div>
<div style="display:none">
	<div id="data">
		<div class="form-group">
			<input type="text" name="nombreusuario" id="nombreusuario" class="form-control" aria-label="Amount (to the nearest dollar)" placeholder="Añade tu nombre de usuario...">
				<a href="#" class="btn input-group-addon btn-secondary" id="enviar_nombreusuario">ENVIAR</a>
		</div>
	</div>
</div>
</body>
</html>