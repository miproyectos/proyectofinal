@extends('base')

@section('contenido')
	<section id="juegos">
		<div class="container">
			<div class="row">
				@foreach ($juegos as $juego)
				
					<div class="col-sm-12 juego" style="background-image: url({{ asset('media/juegos/'.$juego -> imagen) }});">
						<div class="contenido">
							<h2>{{ $juego -> nombre }}</h2>
							<h4>{{ $juego -> categoria }}</h4>
							<p>{{ $juego -> descripcion }}</p>
						</div>
						<div class="botones">
							<a src="#" style="background: #920000"><img src="{{asset('css/iconos/share.png')}}" alt=""></a>
							<a src="#" style="background: #D03F01"><img src="{{asset('css/iconos/look.png')}}" alt=""></a>
							<a src="#" style="background: #00A245"><img src="{{asset('css/iconos/king.png')}}" alt=""></a>
						</div>
						<div class="playvideo">
							<img class = "icon-play" src="{{ asset('css/iconos/play-button.png') }}" style="pointer-events: none;">
							<div class="urlvideo" style="display: none">{{ $juego -> url_video }}</div>
						</div>
					</div>

				@endforeach	
			</div>
		</div>
	</section>
	<script>
	$( document ).ready(function() {

		window.iframe_video_juego = '<iframe class="video" style="display:none"></iframe>';

			$(document).on("click", function(e)
			{
				//Si se pincha en la caja del video (que no este activo)
				if($(e.target).prop('class') == 'playvideo')
				{
					$(".playvideo.active").each(function()
					{
						ocultarVideos(1000, $(this));
					});

					//Se muestran todos los icon play por si pinchamos en otro playvideo
					$(".icon-play").css("opacity",1);
					//Se oculta el icon play del que pinchamos
					$(e.target).find(".icon-play").css("opacity",0);

					$(e.target).addClass('active');

					window.setTimeout(function(){
						$video = $(window.iframe_video_juego).show(0);
						$(e.target).append($video);
						console.log();
						$video.attr("src",$(e.target).find(".urlvideo").html()+"?rel=0&autoplay=1&fs=1");

						window.setTimeout(function()
						{
							$video.css('opacity',1);
						},500);

					},700);

					e.preventDefault();
					e.stopPropagation();
					$(".video").css( "opacity", 1 );

					return false;
				}
				else if($(e.target).prop('class') == 'playvideo active')
				{

				}
				else
				{
					ocultarVideos(1000);
				}
			});



	});

	function ocultarVideos(tiempo, playvideo)
	{
		//Ocultar un video en concreto
		if(playvideo !== undefined)
		{
			$(playvideo).removeClass('active');
			$(playvideo).find('.icon-play').css("opacity",1);

			window.setTimeout(function()
			{
				$(playvideo).find(".video").css( "opacity", 0 );
				window.setTimeout(function()
				{
					$(playvideo).find('.video').remove();
				},tiempo);
			},tiempo);
		}

		//Ocultar todos los videos
		else
		{
			$(".playvideo").removeClass('active');
			$(".icon-play").css("opacity",1);

			//window.clearTimeout(window.referencia);
			//window.referencia = ((impidir el los abusivos clickeos)) 
			window.setTimeout(function()
			{
				$(".video").css( "opacity", 0 );
				window.setTimeout(function()
				{
					$('.video').remove();
				},tiempo);
			},tiempo);
		}
	}
	</script>
@stop