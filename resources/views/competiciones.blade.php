@extends('base')

@section('contenido')
<section id="listadocompeticiones">
	<div class="container">
		<div class="row">
			<div class="col-sm-3 filtro">

		        <?= Form::open(['class' => 'formfiltro', 'method' => 'post']) ?>
		        <?= Form::label('tipocomp', 'Tipo Competicion: ') ?>
		        <?= Form::select('tipocomp',['' => '--eliga una competicion--', '1' => 'competicion oficial', '2' => 'competicion local'],['default'=>(isset($tipo_competicion))? 
		        	 ($tipo_competicion=='oficial')? '1' : 
		        	 (($tipo_competicion=='local')? '2' :'')
		        :''])?>
		        
		        <?= Form::label('titulocomp', 'Titulo: ') ?>
		        <?= Form::text('titulocomp')?>

		     	<?= Form::label('juegoscomp', 'juegos: ') ?>
		        <?= Form::select('juegoscomp',$juegos)?>
		           
				<?= Form::label('limitecomp', 'limite: ') ?>
		        <?= Form::number('limitecomp')?>

		        <?= Form::radio('estado', '0', true,['id'=>'todoscomp'])?>
		        <?= Form::label('todoscomp','todos') ?>
		        <?= Form::radio('estado', '1',false,['id'=>'acabadocomp'])?>
		        <?= Form::label('acabadocomp','acabado') ?>
		        <?= Form::radio('estado', '2',false,['id'=>'esperacomp'])?>
		        <?= Form::label('esperacomp','espera') ?>
		        <?= Form::radio('estado', '3',false,['id'=>'onlinecomp'])?>
		        <?= Form::label('onlinecomp','online') ?>

				<?= Form::label('fecha_iniciocomp', 'fecha inicio: ') ?>
		        <?= Form::date('fecha_iniciocomp')?>

				<?= Form::label('fecha_fincomp', 'fecha fin: ') ?>
		        <?= Form::date('fecha_fincomp')?>

		        <?= Form::label('equipocomp', 'equipo: ') ?>
		        <?= Form::text('equipocomp')?>
		        
		        <?= Form::submit('buscar')?>
		        <?= Form::close() ?>

			</div>
			<div class="col-sm-offset-3 ">
				<div class="row listado">
					
				</div>
			</div>
		</div>
	</div>
</section>

<script type="text/javascript">
	$(document).ready(function()
	{
		$(".formfiltro").submit(function(e)
		{		
			$.ajax(
			{
				url:"/async/filtrocompeticiones",
				type: "POST",
				datatype: 'json',
				data: {
					tipo_competicion: $("#tipocomp").val(),
					titulo: $("#titulocomp").val(),
					juego: $("#juegoscomp").val(),
					limite_apuesta: $("#limitecomp").val(),
					fecha_inicio: $("#fecha_iniciocomp").val(),
					fecha_fin: $("#fecha_fincomp").val(),
					equipo: $("#equipocomp").val(),
					estado: $("#estado:checked").val(),
					_token : "<?=csrf_token()?>"
				},
				success: function(data) 
				{
					var partida = "";
					
					$(".listado").html(partida);

					$.each(data, function(i, val) 
					{
						partida = '<div class="col-sm-4"><a href="/vercompeticion/'+val.id+'">'
						+'<h2>'+val.titulo+'</h2>'
						+'<h4>'+val.nombre_juego+'</h4>'
						+'</a></div>';

						$(".listado").append(partida);
					});

				},
				error: function() 
				{
					alert("error en el servidor");
				}   			
			});

			return false;
		});

		$(function() 
		{
                $("#titulocomp").autocomplete(
                {
                    source: "/async/mostrarcompeticionesauto/",
                    minLength: 1,
                    select: function( event, ui ) 
                    {
                        $('#response').val(ui.item.value);
                    }
                });
            });

		$(".formfiltro").submit();

	});		
</script>
@stop