@extends('base')

@section('contenido')
<style type="text/css">
<!--
.chat_wrapper {
	width: 500px;
	margin-right: auto;
	margin-left: auto;
	background: #CCCCCC;
	border: 1px solid #999999;
	padding: 10px;
	font: 12px 'lucida grande',tahoma,verdana,arial,sans-serif;
}
.chat_wrapper .message_box {
	background: #FFFFFF;
	height: 150px;
	overflow: auto;
	padding: 10px;
	border: 1px solid #999999;
}
.chat_wrapper .panel input{
	padding: 2px 2px 2px 5px;
}
.system_msg{color: #BDBDBD;font-style: italic;}
.user_name{font-weight:bold;}
.user_message{color: #88B6E0;}
-->
</style>

<section id="videotwitch">

	<object type="application/x-shockwave-flash" height="378" width="620" id="live_embed_player_flash" data="http://es.twitch.tv/widgets/live_embed_player.swf?channel=chaoxlol" bgcolor="#000000"><param name="allowFullScreen" value="true" /><param name="allowScriptAccess" value="always" /><param name="allowNetworking" value="all" /><param name="movie" value="http://es.twitch.tv/widgets/live_embed_player.swf" /><param name="flashvars" value="hostname=es.twitch.tv&channel=chaoxlol&auto_play=true&start_volume=25" /></object>

	<a href="http://es.twitch.tv/chaoxlol">Watch live video from tsm_chaosx on es.twitch.tv</a>	

</section>
<section id="vercompeticionapuesta">
	
</section>
<section id="vercompeticiondescripcion">
	
</section>
<section id="chatgeneral">

<div class="chat_wrapper">
<div class="message_box" id="message_box"></div>
<div class="panel">

<form action="">
	<input type="text" name="message" id="message" placeholder="Message" maxlength="80" style="width:60%" />
	<button type="submit" id="send-btn">Send</button>
</form>
</div>
<h2>usuarios</h2>
<hr>
<div id="usuarios">
	
</div>
</div>

</section>
<script>
$(document).ready(function(){
	
	var nomusuario = "";

	/*
	notificaciones de chrome
	 */

	function notifyMe() {
	  // Let's check if the browser supports notifications
	  if (!("Notification" in window)) {
	    alert("This browser does not support system notifications");
	  }

	  // Let's check whether notification permissions have already been granted
	  else if (Notification.permission === "granted") {
	    // If it's okay let's create a notification
	    var notification = new Notification("Hi there!");
	  }

	  // Otherwise, we need to ask the user for permission
	  else if (Notification.permission !== 'denied') {
	    Notification.requestPermission(function (permission) {
	      // If the user accepts, let's create a notification
	      if (permission === "granted") {
	        var notification = new Notification("Hi there!");
	      }
	    });
	  }

	  // Finally, if the user has denied notifications and you 
	  // want to be respectful there is no need to bother them any more.
	}

	function notifyMe(theBody,theIcon,theTitle) {
	  if(document.hidden){
		  if (Notification.permission === "granted") {
		    	  var options = {
			      body: theBody,
			      icon: theIcon
			  }
			  var n = new Notification(theTitle,options);
			  setTimeout(n.close.bind(n), 5000); 
			  } 	
	  }	

	}
	//create a new WebSocket object.
	var wsUri = "ws://localhost:8080/chat/servidor.php"; 	
	window.websocket = new WebSocket(wsUri);
	window.chatColor = '000000';

	websocket.onmessage = function(e){

		var obj = JSON.parse(e.data);

		if(obj.code == "send_message"){
			
			notifyMe(obj.message,"img.jpg",obj.name);

			$("#message_box").append("<p><span style='color: #"+obj.color+"' >" + obj.emisor + "</span> : " + obj.message + "</p>");
		}
		else if(obj.code == "set_color")
		{
			window.chatColor = obj.color;
		}
		else if(obj.code == "show_users"){

			$("#usuarios").html(obj.usuarios.join("<br>"));
		}
	}

	websocket.onclose = function(){   	

		$("#message_box").append("<p>se perdio la conexion con el servidor de chat</p>");
	}

	websocket.onerror = function(e){
		
		var obj = JSON.parse(e.data);    	

		$("#message_box").append("<p>"+e+"</p>");

	}
	
	$("#enviar_nombreusuario").click(function(){

		if($("#nombreusuario").val() != ""){

			nomusuario= $("#nombreusuario").val();
			//prepare json data
			var msg = {
				code: 'loggin',
				name: nomusuario,
			};
			//convert and send data to server
			websocket.send(JSON.stringify(msg));

			$.fancybox.close();	
		}
	});

	$('#send-btn').click(function(a){ //use clicks message send button	

		a.preventDefault();

		var mymessage = $('#message').val(); //get message text
		
		if(nomusuario == ""){ //empty name?
			alert("Enter your Name please!");
			return;
		}
		if(mymessage == ""){ //emtpy message?
			alert("Enter Some message Please!");
			return;
		}

		//prepare json data
		var msg = {
			code: 'send_message',
			message: mymessage,
		};
		//convert and send data to server
		websocket.send(JSON.stringify(msg));

		$("#message_box").append("<p><span style='color: #" + window.chatColor + "' >" + nomusuario + "</span> : " + mymessage + "</p>");
	});

});
</script>

@stop