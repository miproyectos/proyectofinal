<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>proyectofinal</title>
	<link rel="stylesheet" href="/css/bootstrap.min.css">
	<link rel="stylesheet" href="/css/estilos.css">
	<link rel="stylesheet" href="/jquery-ui.min.css">
  	<script src="/jquery.min.js"></script>
  	<script src="/jquery-ui.min.js"></script>
	

</head>
<body>
<?php $pagina = Route::getCurrentRoute()->getPath() ?>

<header class="{{ ($pagina!='/') ? 'interior' : '' }}" data-spy="affix" data-offset-top="1">
	<div class="container">
		<div class="row">
			<nav>
				<ul>
					<li><a class="{{ ($pagina=='/') ? 'active' : '' }}" href="/">Inicio</a></li>
					<li><a class="{{ ($pagina=='juegos') ? 'active' : '' }}" href="/juegos">Juegos</a></li>
					<li><a  href="foros" >Foros</a></li>
					<li class="competiciones" ><a class="{{ ($pagina == 'competiciones') ? 'active' : '' }} " href="#" >Competiciones</a>
							<div class="submenu">
								<ul>
									<li>
										<a href="/competiciones/oficial">Competiciones Oficiales</a>
									</li>
									<li>
										<a href="/competiciones/local">Competiciones Locales</a>
									</li>
								</ul>
							</div>
					</li>
					<li><a class="{{ ($pagina=='contacto') ? 'active' : '' }}" href="">Contacto</a></li>
					<li class="loginopen"><a href="">Login</a>
						<div class="login">
		        			<?= Form::open(['class' => 'formlogin', 'method' => 'post', 'action'=>'Auth\AuthController@postLogin']) ?>
			        			<?= Form::label('userlogin', 'Usuario: ') ?>
			        			<?= Form::email('userlogin') ?>

			        			<?= Form::label('passwordlogin', 'Contraseña: ') ?>
			        			<?= Form::password('passwordlogin') ?>

			        			<?= Form::submit(); ?>
		        			<?= Form::close() ?>
						</div>
					</li>
				</ul>
			</nav>
		</div>
	</div>
</header>	
	@yield('contenido')
  <script src="/bootstrap.min.js"></script>

</body>
</html>
