@extends('base')

@section('contenido')
@if(Auth::check())
    <div>esto es una prueba la contraseña esta good</div>
    {{ Auth::logout() }}
@endif
<section id="portada">
	<div class="contentvideo">
		<div class="fondooscuro"></div>
		
	</div>
	<div class="container">
		<div class="row">
			<div class="col-sm-6 col-sm-offset-3 contenido">
				<h1>Apuesta por lo que te gusta</h1>
				<h2>Demuestra que tu tienes la razón</h2>
				<a href="">Empieza ya</a>
			</div>
		</div>
	</div>
</section>

<section id="ventajas">
<div class="container">
	<div class="row">
		<div class="col-sm-4">
			<div class="imagen">
				<img src="../imagenes/escudoespada.png" alt="">
			</div>
			<h3>Los mejores</h3>
			<p>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
			</p>
		</div>
		<div class="col-sm-4">
			<div class="imagen">
				<img src="../imagenes/cup.png" alt="">
			</div>
			<h3>Los mejores</h3>
			<p> 
				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
			</p>
		</div>
		<div class="col-sm-4">
			<div class="imagen">
				<img src="../imagenes/money.png" alt="">				
			</div>
			<h3>Los mejores</h3>
			<p> 
				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
			</p>
		</div>
	</div>	
</div>
</section>

<section id="quienessomos">
	<div class="container">
		<div class="row">
			<div class="col-sm-6 col-xs-6">
				<h2>Quienes somos</h2>
				<h4>Conozca a los mejores</h4>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. In nam amet omnis aliquam officiis porro asperiores hic nobis quos accusantium! Dolorem autem facilis perspiciatis excepturi! Voluptate, sint, repellendus. Saepe, ut!</p>
				<a href="">Conocenos</a>
			</div>	
			<div class="col-sm-6 col-xs-6 tutorial">
					<h2>¿Como Funciona?</h2>
					<h4>Sigue los pasos del tutorial</h4>
				<div class="col-sm-6">
					<ul>
						<li>selecciona una partida</li>
						<li>apuesta por uno de los equipos</li>
						<li>chatea con los demas apostantes</li>
						<li>gana dinero real</li>
					</ul>
				</div>
				<div class="col-sm-6">
					<a href=""><img src="../imagenes/tutorial.png" alt=""></a>
				</div>
			</div>
		</div>	
	</div>
</section>

<section id="proximoseventos">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h2 class="tituloseccion" >Proximos eventos</h2>
			</div>
			<div class="eventos">
				@foreach ($eventos as $evento)
					<div class="col-sm-4">		
						<div class="imagen" style="background-image: url({{ asset('media/eventos/'.$evento->imagen) }});">
							<div class="fondofecha">
								<div class="fecha">{{ $evento->fecha_anunciar->formatLocalized('%e %B') }}</div>
							</div>
						</div>
						<h3>{{ $evento->titulo }}</h3>
						<p class="subtitulo">iure placeat vel aspernatur! Perferendis archite <span><i></i></span></p>
						<p>ptatem qui debitis autem! Consequatur nemo iure placeat vel aspernatur! Perferendis architecto delectus ipsam!</p>
						<a href="">ver mas</a>
					</div>				
				@endforeach
			</div>
		</div>		
	</div>	
</section>

<section id="ultimoscampeonatos">
	<div class="container">
		<div class="row">
		<div class="col-sm-12">
			<h2 class="tituloseccion">Ultimos campeonatos oficiales</h2>
		</div>	
		</div>		
	</div>
</section>

<footer>

</footer>


<!-- BigVideo Dependencies -->
<script src="//vjs.zencdn.net/4.3/video.js"></script>

<!-- BigVideo -->
<script src="../bower_components/BigVideo/lib/bigvideo.js"></script>


<script>
	$(function() {
	    var BV = new $.BigVideo({
	    	useFlashForFirefox:false,
            container:$('.contentvideo')//Container

	    });
		BV.init();
	    BV.show('../video/video.mp4',{ambient:true});
	    
	});
</script>

@stop